var mongo = require("mongodb");

module.exports = {
	findDocuments: function(collection, unusedParameters, callback) {
		collection.find({}).toArray(function(err, documentArray) {
			if (err) { console.log(err); }
			else { callback(documentArray); }
		});
	},
	saveDocument: function(collection, documentToSave, callback) {
		collection.save(documentToSave, function(err, saved) {
			if (err) { console.log(err); }
			else { callback(documentToSave); }
		});
	},	
	connectToMongo: function(url, operation, operationParameters, errorReturnValue, callback) {
		mongo.MongoClient.connect(url, function(err, db) {
			if (err) { 
				console.log("Error connecting to server");
				callback(errorReturnValue);
			} else {			
				console.log("Connected successfully to server");
				var collection = db.collection('documents');
				
				operation(collection, operationParameters, function(result) {
					db.close();
					callback(result);
				});
			}
		});
	},
};