var express = require('express');
var bodyParser = require("body-parser");
var manager = require('./adManager');

const HTML_CONTENT_TYPE = 'text/html';

var app = express();
app.use(bodyParser.urlencoded({ extended: false }));

var SendResponse = function(data, response) {
	response.writeHead(200, {
		'Content-Type': HTML_CONTENT_TYPE, 
		'Access-Control-Allow-Origin': 'http://localhost:8100' 	// Allow only the ionic "Grocery List" app on port 8100 to use this API
		// 'Access-Control-Allow-Origin': '*' 					// Allow all sources to use this API
	});
	response.end(data.toString());
}

var SendErrorResponse = function(response) {
	response.writeHead(404, {'Content-Type': HTML_CONTENT_TYPE});
	response.end();
}

app.get('/Ads', function (request, response) {
	 manager.GetAds(function(adArray) {
		adArrayAsJSON = JSON.stringify(adArray);
		SendResponse(adArrayAsJSON, response);
	});
})

app.get('/Ads/:adID', function (request, response) {
	var adID = request.params.adID;
	
	manager.GetAd(adID, function(ad) {
		adAsJSON = JSON.stringify(ad);
		SendResponse(adAsJSON, response);
	});
})

app.post('/Ads', function(request, response) {
	var name = request.body.name;
	var price = request.body.price;
	
	manager.CreateAd(name, price, function(ad) {
		//console.log(ad);
		adAsJSON = JSON.stringify(ad);
		SendResponse(adAsJSON, response);
	});
});

var server = app.listen(7225, function () {
	console.log("Grocery Store Ad Service app listening at http://localhost:%s", server.address().port);
})
