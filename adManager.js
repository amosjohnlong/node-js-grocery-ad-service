var fs = require("fs");
var xml2js = require("xml2js");
var utility = require('./mongoUtility');

var MONGO_AD_URL = 'mongodb://localhost:27017/grocerystoreads';
var DEFAULT_AD_ARRAY = 
	[{name: 'Skim Milk', price: '$2.89'}, 
	 {name: 'Soup', price: '10 for $7'},
	 {name: 'Onions', price: '$0.99'},
	 {name: 'Cap \'n Crunch', price: '$3.29'}];

module.exports = {
	GetAds: function(callback) {
		utility.connectToMongo(MONGO_AD_URL, utility.findDocuments, null, DEFAULT_AD_ARRAY,
			function(adArray) {	callback(adArray); });
	},
	GetAd: function(index, callback) {
		utility.connectToMongo(MONGO_AD_URL, utility.findDocuments, null, DEFAULT_AD_ARRAY[index],
			function(ad) {	callback(ad); });
	},
	CreateAd: function(name, price, callback) {
		var documentToSave = {name: name, price: price};
		
		utility.connectToMongo(MONGO_AD_URL, utility.saveDocument, documentToSave, null,
			function(ad) { callback(ad); });
	},
};
